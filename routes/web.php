<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Catalog
Route::get('/catalog', 'ItemController@index');

// add item
Route::get('/additem', 'ItemController@create');

//to save
Route::post('/additem', 'ItemController@store');

// to delete
Route::delete('/deleteitem/{id}', 'ItemController@destroy');

// to edit
Route::get('/edititem/{id}', 'ItemController@edit');

// to save edit
Route::patch('/edititem/{id}','ItemController@update');

// Cart CRUD
Route::post('/addtocart/{id}', 'ItemController@addToCart');

// show cart
Route::get('/showcart', 'ItemController@showCart');