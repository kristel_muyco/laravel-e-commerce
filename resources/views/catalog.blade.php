@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Product List</h1>

@if(Session::has("message"))
	<h4>{{Session::get('message')}}</h4>
@endif

<div class="container">
	<div class="row">
			@foreach ($items as $indiv_item)
				<div class="col-lg-4 py-2">
					<div class="card">
						<img class="card-img-top" alt="Nothing" height="300px" 
							src="{{$indiv_item->imgPath}}" alt="">
						<div class="card-body">
							<h4 class="card-title">{{$indiv_item->name}}</h4>
							<p class="card-text">Price: Php {{$indiv_item->price}}</p>
							<p class="card-text">Description: {{$indiv_item->description}}</p>
							<p class="card-text">Category: {{$indiv_item->category->name}}</p>
						<form action="/deleteitem/{{$indiv_item->id}}" method="POST">
						@csrf
						@method('DELETE')
							<button class="btn btn-danger" type="submit">Delete Item</button>
							<a href="/edititem/{{$indiv_item->id}}" class="btn btn-primary ">Edit Item</a>
						</form>
						</div>
						<div class="card-footer">
							<form action="/addtocart/{{$indiv_item->id}}" method="POST">
							@csrf
							<input type="number" name="quantity" class="form-control" value="1">
							<button class="btn btn-primary" type="submit">Add to Cart</button>
							</form>
						</div>
					</div>
				</div>
		@endforeach
	</div>
</div>
@endsection